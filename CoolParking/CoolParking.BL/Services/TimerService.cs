﻿using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using System;
using System.Timers;

namespace CoolParking.BL.Services
{
    public class TimerService : ITimerService
    {
        private double interval;
        public double Interval { get => interval; set => interval = value; }
        public Timer timer;
        public event ElapsedEventHandler Elapsed;
        public void Dispose()
        {
            timer.Dispose();
        }
        public void Start()
        {
            timer = new Timer();
            timer.AutoReset = true;
            timer.Interval = interval;
            timer.Elapsed += Elapsed;
            timer.Enabled = true;
            timer.Start();
        }
        public void Stop()
        {
            timer.Stop();
        }
    }
}