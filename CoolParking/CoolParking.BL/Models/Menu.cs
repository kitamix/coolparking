﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CoolParking.BL.Services;

namespace CoolParking.BL.Models
{
    public static class Menu
    {
        private static ParkingService _parkingService = new ParkingService(
            new TimerService(),
            new TimerService(),
            new LogService(Settings.LogFilePath));
        public static void Start()
        {
            ClearAndBack.Clear();
            Console.WriteLine("---###MENU###---");
            Console.WriteLine("1. Show parking balance");
            Console.WriteLine("2. Show income for the current period");
            Console.WriteLine("3. Show parking capacity");
            Console.WriteLine("4. Show transactions for the current period");
            Console.WriteLine("5. Show transactions history");
            Console.WriteLine("6. Show vehicles, currently placed on parking");
            Console.WriteLine("7. Place vehicle on parking");
            Console.WriteLine("8. Remove vehicle on parking");
            Console.WriteLine("9. Top up vehicle balance");
            Console.WriteLine("10. Exit");
            Console.Write("\nWrite choosen menu option: ");
            int menu_key = Int32.Parse(Console.ReadLine());
            switch (menu_key)
            {
                case 1: 
                    ShowParkingBalance();
                    break;
                case 2:
                    ShowCurrentIncome();
                    break;
                case 3:
                    ShowParkingCapacity();
                    break;
                case 4:
                    ShowCurrentTransactions();
                    break;
                case 5:
                    ShowTransactionsHistory();
                    break;
                case 6:
                    ShowVehicles();
                    break;
                case 7:
                    PlaceVehicle();
                    break;
                case 8: 
                    RemoveVehicle();
                    break;
                case 9:
                    TopUpVehicle();
                    break;
                case 10:
                    Environment.Exit(0);
                    break;
                default:
                    throw new Exception("Choosen munu option must be in range 1 to 10");

            }
        }
        public static void ShowParkingBalance()
        {
            ClearAndBack.Clear();
            Console.WriteLine($"Current parking balance: {_parkingService.GetBalance()}");
            ClearAndBack.Back();
        }
        public static void ShowCurrentIncome()
        {
            ClearAndBack.Clear();
            Console.WriteLine($"Current parking income: {_parkingService.GetCurrentIncome()}");
            ClearAndBack.Back();
        }
        public static void ShowParkingCapacity()
        {
            ClearAndBack.Clear();
            Console.WriteLine($"Free places on parking: {_parkingService.GetFreePlaces()}\n" +
                $"Occupied places on parking: {_parkingService.GetCapacity() - _parkingService.GetFreePlaces()}\n" +
                $"Total places on parking: {_parkingService.GetCapacity()}");
            ClearAndBack.Back();
        }
        public static void ShowCurrentTransactions()
        {
            ClearAndBack.Clear();
            var transactions = _parkingService.GetLastParkingTransactions();
            foreach (var transaction in transactions)
            {
                Console.WriteLine(transaction.TransactionMessage);
            }
            ClearAndBack.Back();
        }
        public static void ShowTransactionsHistory()
        {
            ClearAndBack.Clear();
            var transactions = _parkingService.ReadFromLog();
            Console.WriteLine(transactions);
            ClearAndBack.Back();
        }
        public static void PlaceVehicle()
        {
            ClearAndBack.Clear();
            Console.Write("Write new vehicle iD it format \"XX-YYYY-XX\", where X - letter, Y - number: ");
            var vehicleId = Console.ReadLine();
            Console.Write("\nChoose Vehicle Type (1 - PassengerCar, 2 - Truck, 3 - Bus, 4 - Motorcycle): ");
            var vehicleType = Int32.Parse(Console.ReadLine()) - 1;
            Console.Write("\nWrite start vehicle balance: ");
            decimal balance = decimal.Parse(Console.ReadLine());
            _parkingService.AddVehicle(new Vehicle(vehicleId, (VehicleType)vehicleType, balance));
            Console.WriteLine("\nVehicle was placed successfully!");
            ClearAndBack.Back();
        }
        public static void ShowVehicles()
        {
            ClearAndBack.Clear();
            VehicleList.List();
            ClearAndBack.Back();
        }
        public static void RemoveVehicle()
        {
            ClearAndBack.Clear();
            VehicleList.List();
            Console.Write("\nChoose Parking Spot number: ");
            var vehicleIndex = Int32.Parse(Console.ReadLine());
            var vehicleId = _parkingService.GetVehicles()[vehicleIndex - 1].Id;
            _parkingService.RemoveVehicle(vehicleId);
            Console.WriteLine("Vehicle removed successfully!");
            ClearAndBack.Back();
        }
        public static void TopUpVehicle()
        {
            ClearAndBack.Clear();
            VehicleList.List();
            Console.Write("\nChoose Parking Spot number: ");
            var vehicleIndex = Int32.Parse(Console.ReadLine());
            var vehicleId = _parkingService.GetVehicles()[vehicleIndex - 1].Id;
            Console.Write("Write top up amount: ");
            var sum = Int32.Parse(Console.ReadLine());
            _parkingService.TopUpVehicle(vehicleId, sum);
            Console.WriteLine("Topped up successfully!");
            ClearAndBack.Back();
        }
        protected class VehicleList
        {
            public static void List()
            {
                var vehicles = _parkingService.GetVehicles();
                var counter = 1;
                Console.WriteLine($"Spot № \t|\tID\t\t|\tBalance\t|\tType");
                foreach (var vehicle in vehicles)
                {
                    Console.WriteLine($"{counter} \t|\t{vehicle.Id}\t|\t{vehicle.Balance}\t|\t{vehicle.VehicleType}");
                    counter += 1;
                }
            }
        }

        protected class ClearAndBack
        {
            public static void Clear()
            {
                Console.Clear();
            }
            public static void Back()
            {
                Console.WriteLine("\nPress any key to go back...");
                Console.ReadKey();
                Start();
            }
        }
    }   
}
