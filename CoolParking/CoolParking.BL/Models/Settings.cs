﻿using System.Collections.Generic;
using System.IO;
using System.Reflection;

namespace CoolParking.BL.Models
{
    public static class Settings
    {
        public static decimal StartBalance = 0;
        public static int ParkingCapacity = 10;
        public static int LogInterval = 60000;
        public static int WithdrawInterwal = 5000;
        public static decimal FineCoefficient = 2.5M;
        public static string LogFilePath = $@"{Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location)}\Transactions.log";
        public static Dictionary<VehicleType, decimal> Withdraw { get; set; } = new Dictionary<VehicleType, decimal>()
        {
            [VehicleType.PassengerCar] = 2M,
            [VehicleType.Truck] = 5M,
            [VehicleType.Bus] = 3.5M,
            [VehicleType.Motorcycle] = 1M,
        };
        
    }
}