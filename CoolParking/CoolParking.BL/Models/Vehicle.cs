﻿using System;
using System.Text.RegularExpressions;

namespace CoolParking.BL.Models
{
    public class Vehicle
    {
        public string Id { get; }
        public VehicleType VehicleType { get; }
        public decimal Balance { get;internal set; }
        public Vehicle(string id, VehicleType vehicleType, decimal balance)
        {
            var regex = new Regex($"[A-z][A-z]-[0-9][0-9][0-9][0-9]-[A-z][A-z]");
            if (regex.IsMatch(id) && balance > 0)
            {
                Id = id.ToUpper();
            }
            else
            {
                throw new ArgumentException("Bad Vehicle ID!");
            }
            VehicleType = vehicleType;
            Balance = balance;
        }
        public static string GenerateRandomRegistrationPlateNumber()
        {
            string id = "";
            Random random = new Random();
            for (int i = 0; i < 4; i++)
            {
                id += (char)random.Next('A', 'Z');
                if (id.Length == 2)
                {
                    id += '-';
                    for(int j = 0; j < 4; j++)
                    {
                        id += (int)random.Next(0, 9);
                    }
                    id += '-';
                }
            }
            return id;
        }
    }
}