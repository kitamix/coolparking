﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using CoolParking.BL.Models;
using CoolParking.BL.Services;
using CoolParking.BL.Interfaces;


namespace CoolParking.ConsoleApp
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Menu.Start();
        }
    }
}
